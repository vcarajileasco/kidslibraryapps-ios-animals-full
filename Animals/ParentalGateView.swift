//
//  ParentalGateView.swift
//  Animals
//
//  Created by Vitali Carajileasco on 7/2/18.
//  Copyright © 2018 Kids' Library Apps. All rights reserved.
//

import UIKit

@IBDesignable class ParentalGateView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

}
