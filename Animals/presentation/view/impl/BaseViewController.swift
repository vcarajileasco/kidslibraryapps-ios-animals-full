//
//  BaseViewController.swift
//  Apps iOS
//
//  Created by Vitali Carajileasco on 12/14/18.
//  Copyright © 2018 workstation. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var oldBarButton: UIBarButtonItem!
    var refreshBarButton: UIBarButtonItem!
    var tagSearchController: UISearchController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.activityIndicatorViewStyle = .gray
        refreshBarButton = UIBarButtonItem(customView: activityIndicator)
        activityIndicator.startAnimating()
        
        if(self.navigationItem.rightBarButtonItem != nil) {
            oldBarButton = self.navigationItem.rightBarButtonItem
        }
        else if(self.tabBarController?.navigationItem.rightBarButtonItem != nil) {
            oldBarButton = self.tabBarController?.navigationItem.rightBarButtonItem
        }
        else if(self.navigationController?.navigationItem.rightBarButtonItem != nil) {
            oldBarButton = self.navigationController?.navigationItem.rightBarButtonItem
        }
    }
    
    func getSearchController() -> UISearchController? {
        return self.tabBarController?.navigationItem.searchController
    }
    
    func setSearchController(searchController: UISearchController) {
        self.tagSearchController = searchController
        self.tabBarController?.navigationItem.searchController = searchController
        self.view.setNeedsLayout()
    }
    
    func showSearchController() {
        if let searchController = tagSearchController {
            setSearchController(searchController: searchController)
        }
    }
    
    func hideSearchController() {
        self.tabBarController?.navigationItem.searchController = nil
        self.view.setNeedsLayout()
    }
    
    func startActivityView() {
        DispatchQueue.main.async {
            self.navigationItem.setRightBarButton(self.refreshBarButton, animated: true)
            self.tabBarController?.navigationItem.setRightBarButton(self.refreshBarButton, animated: true)
            self.navigationController?.navigationItem.setRightBarButton(self.refreshBarButton, animated: true)
        }
    }
    
    func stopActivityView() {
        DispatchQueue.main.async {
            self.navigationItem.setRightBarButton(self.oldBarButton, animated: true)
            self.tabBarController?.navigationItem.setRightBarButton(self.oldBarButton, animated: true)
            self.navigationController?.navigationItem.setRightBarButton(self.oldBarButton, animated: true)
        }
    }
    
    func showMessage(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration: Double = 5
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        }
    }
    
    func showMessage(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration: Double = 5
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        }
    }
    
    func showMessageDialog(title: String, message: String,
                           okEvent: ((UIAlertAction) -> Void)? = nil,
                           cancelEvent: ((UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: okEvent))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: cancelEvent))
            
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration: Double = 5
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    @objc func hideKeyboard() {
        DispatchQueue.main.async {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
        }
    }
    
}
