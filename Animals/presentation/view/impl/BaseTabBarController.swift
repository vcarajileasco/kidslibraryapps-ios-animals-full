//
//  BaseTabBarController.swift
//  Apps iOS
//
//  Created by Vitali Carajileasco on 12/21/18.
//  Copyright © 2018 workstation. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {
    var tagSearchController: UISearchController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //setSwipeAnimation(type: SwipeAnimationType.sideBySide)
        //setDiagonalSwipe(enabled: false)
    }
    
    func setSearchController(searchController: UISearchController) {
        self.tagSearchController = searchController
        self.tabBarController?.navigationItem.searchController = searchController
        self.view.setNeedsLayout()
    }
    
    func showSearchController() {
        if let searchController = tagSearchController {
            setSearchController(searchController: searchController)
        }
    }
    
    func hideSearchController() {
        self.tabBarController?.navigationItem.searchController = nil
        self.view.setNeedsLayout()
    }
    
    func showMessage(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration: Double = 5
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        }
    }
    
    func showMessage(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration: Double = 5
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        }
    }
    
    func showMessageDialog(title: String, message: String,
                           okEvent: ((UIAlertAction) -> Void)? = nil,
                           cancelEvent: ((UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: okEvent))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: cancelEvent))
            
            self.present(alert, animated: true)
            
            // duration in seconds
            let duration: Double = 25
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        }
    }
    
    func showMessageDialog(title: String, message: String,
                           okEvent: ((UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: okEvent))
            
            self.present(alert, animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func removeTabBarItem(item: Int) {
        DispatchQueue.main.async {
            if item < self.viewControllers!.count {
                var viewControllers = self.viewControllers
                viewControllers?.remove(at: item)
                self.viewControllers = viewControllers
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
