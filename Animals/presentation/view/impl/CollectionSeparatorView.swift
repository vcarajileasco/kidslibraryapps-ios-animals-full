//
//  CollectionSeparatorView.swift
//  Apps iOS
//
//  Created by workstation on 2/27/19.
//  Copyright © 2019 workstation. All rights reserved.
//

import UIKit

class CollectionSeparatorView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        frame = layoutAttributes.frame
    }
}
