//
//  GamesTableViewController.swift
//  Animals Full
//
//  Created by Vitali Carajileasco on 5/25/19.
//  Copyright © 2019 Kids' Library. All rights reserved.
//

import UIKit

class GamesTableViewController: BaseTableViewController {
    @IBOutlet var gamesTableView: UITableView!
    
    // objects
    var settings = SettingsApp()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        // enable notification
        NotificationCenter.default.addObserver(self, selector: #selector(applyCurrentSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // apply current settings
        applyCurrentSettings()
        
        // set navigation visible
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func applyCurrentSettings() {
        // read current settings
        self.settings.readSettings()
        
        // set navigation bar  title
        navigationItem.title = ProjectText.categories[0].localized(lang: settings.app_language!)
        
        // reload table view content
        self.gamesTableView?.reloadData()
    }
    
    func openGame(index: Int) {
        // open slider with index
        if(index == 0) {
            let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "GameSelectController") as? GameSelectController
            
            self.navigationController?.pushViewController(vc!, animated:true)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ProjectText.games.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryViewCell
        
        // Configure the cell...
        cell.cellLabel?.text = ProjectText.games[indexPath.row].localized(lang: settings.app_language!)
        cell.cellImage?.image = UIImage(named: ProjectImages.games[indexPath.row])
        
        // prepare images
        //cell.cellImage.layer.cornerRadius = 20.0
        //cell.cellImage.clipsToBounds = true
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        openGame(index: index)
    }
    
}
