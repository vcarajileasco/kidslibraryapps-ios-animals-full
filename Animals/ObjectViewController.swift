//
//  SecondViewController.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import UIKit

class ObjectViewController: UIViewController {
    // views
    @IBOutlet weak var itemView: UIStackView!
    
    // components fom view
    @IBOutlet weak var objectLabel: UILabel!
    @IBOutlet weak var objectImage: UIImageView!
    @IBOutlet weak var objectDescription: UITextView!
    
    // variables
    var lastObjectState: Bool = false
    
    // content
    var itemIndex: Int = 0
    
    var lableName: String = ""
    var imageName: String = ""
    var descriptionName: String = ""
    
    var soundName: String = ""
    var soundItem: String = ""
    
    // use player
    var newPlayer = Player()
    
    // custom settings
    var settings = SettingsApp()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        enableImageGestures()
        
        // enable notification
        NotificationCenter.default.addObserver(self, selector: #selector(applyCurrentSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // apply current settings
        applyCurrentSettings()
    }
    
    @objc func applyCurrentSettings() {
        // read current settings
        settings.readSettings()

        // set card's name
        self.objectLabel.text = lableName.localized(lang: settings.app_language!)
        
        if settings.cards_name_upper! {
            self.objectLabel.text = self.objectLabel.text?.uppercased()
        }
        
        // set card's name size and color
        self.objectLabel.font = self.objectLabel.font.withSize(CGFloat(settings.pref_card_name_size!))
        switch settings.pref_cards_name_color! {
        case "gray":
            self.objectLabel.textColor = UIColor.gray
        case "blue":
            self.objectLabel.textColor = UIColor.blue
        case "red":
            self.objectLabel.textColor = UIColor.red
        default:
            self.objectLabel.textColor = UIColor.black
        }
        
        // set card's description size
        self.objectDescription.font = self.objectDescription.font?.withSize(CGFloat(settings.pref_card_descip_size!))
        
        // set content
        self.objectImage.image = UIImage(named: imageName)
        self.objectDescription.text = descriptionName.localized(lang: settings.app_language!)
        
        // save last object state
        lastObjectState = !objectDescription.isHidden
        
        // show slide in needed orientation
        prepareCurrentOrientationDevice()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        prepareCurrentOrientationDevice()
    }
    
    func prepareCurrentOrientationDevice() {
        // get current orientation
        let orientation = UIDevice.current.orientation
        
        if orientation == .landscapeLeft || orientation == .landscapeRight {
            // show or hide Object description
            showObjectDescription(state: false, label: false)
        }
        else if orientation == .portrait {
            // show or hide Object description
            showObjectDescription(state: lastObjectState, label: true)
        }
    }
    
    func enableImageGestures() {
        self.objectImage.isUserInteractionEnabled = true
        
        // create 2 tap listener
        let doubleClickTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageDoubleClickGesture) )
        doubleClickTapGesture.numberOfTapsRequired = 2
        self.objectImage.addGestureRecognizer(doubleClickTapGesture)
        
        // create 1 tap listener
        let clickTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageClickGesture) )
        clickTapGesture.numberOfTapsRequired = 1
        clickTapGesture.require(toFail: doubleClickTapGesture)
        self.objectImage.addGestureRecognizer(clickTapGesture)
        
        
        // create long tap listener
        let longClickTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.imageLongClickGesture) )
        self.objectImage.addGestureRecognizer(longClickTapGesture)
    }
    
    
    @objc func imageDoubleClickGesture(gesture: UIGestureRecognizer) {
        // prepare double tap listene
        if (!objectLabel.isHidden) &&
            settings.cards_description_show! &&
            !self.objectDescription.text.isEmpty {
            
            // get current Object state
            let hiden = objectDescription.isHidden
            
            // show or hide Object description
            showObjectDescription(state: hiden, label: true)
            
            // save last Object state
            lastObjectState = !objectDescription.isHidden
        }
    }
    
    func showObjectDescription (state: Bool, label: Bool) {
        // change Object description
        objectDescription.isHidden = !state
        objectLabel.isHidden = !label
        
        // show or hide description
        if state {
            // redraw item view
            itemView.distribution = UIStackViewDistribution.fill
        }
        else {
            // redraw item view
            itemView.distribution = UIStackViewDistribution.fillProportionally
        }
    }
    
    @objc public func imageClickGesture(gesture: UIGestureRecognizer) {
        // prepare 1 tap listener
        playObjectName(play: settings.sound_names_cards!)
    }
    
    public func playObjectName(play: Bool) {
        if play {
            self.newPlayer.Play(soundName: soundName, loop: false, localized: true)
        }
    }
    
    @objc func imageLongClickGesture(gesture: UILongPressGestureRecognizer) {
        // prepare 1 tap listener
        if settings.sound_cards! {
            self.newPlayer.Play(soundName: soundItem, loop: false, localized: false)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
