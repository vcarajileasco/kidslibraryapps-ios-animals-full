//
//  ParentalGateController.swift
//  Animals
//
//  Created by Vitali Carajileasco on 7/2/18.
//  Copyright © 2018 Kids' Library Apps. All rights reserved.
//

import UIKit

protocol ParentalGateControllerDelegate
{
    func ParentalGateControllerResponse(success: Bool)
}

class ParentalGateController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLable: UILabel!
    
    @IBOutlet weak var lock1: UIImageView!
    @IBOutlet weak var lock2: UIImageView!
    @IBOutlet weak var lock3: UIImageView!
    
    private var unlock: Int = 0
    
    public var app_language: String?
    
    var delegate: ParentalGateControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel?.text = ProjectText.parentsGate[0].localized(lang: app_language)
        messageLable?.text = ProjectText.parentsGate[1].localized(lang: app_language)

        EnableGestureRecognize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissParentalGate(_ sender: UIButton) {
        DismisParentalGate(success: false)
    }
    
    
    func EnableGestureRecognize() {
        self.lock1.isUserInteractionEnabled = true
        self.lock2.isUserInteractionEnabled = true
        self.lock3.isUserInteractionEnabled = true
        
        // create long tap listener
        let long1ClickTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.image1LongClickGesture) )
        self.lock1.addGestureRecognizer(long1ClickTapGesture)

        let long2ClickTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.image2LongClickGesture) )
        self.lock2.addGestureRecognizer(long2ClickTapGesture)
        
        let long3ClickTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.image3LongClickGesture) )
        self.lock3.addGestureRecognizer(long3ClickTapGesture)
    }
    
    
    @objc func image1LongClickGesture(gesture: UILongPressGestureRecognizer) {
        // prepare long tap listener
        if gesture.state == .began {
            lock1.image = UIImage(named: "lock_pressed")
            unlock = unlock + 1
        }
        else if gesture.state == .ended {
            if unlock == 3 {
                SuccesVerified()
            }
            else {
                unlock = unlock - 1
                lock1.image = UIImage(named: "lock")
            }
        }
    }
    
    @objc func image2LongClickGesture(gesture: UILongPressGestureRecognizer) {
        // prepare long tap listener
        if gesture.state == .began {
            lock2.image = UIImage(named: "lock_pressed")
            unlock = unlock + 1
        }
        else if gesture.state == .ended {
            if unlock == 3 {
                SuccesVerified()
            }
            else {
                unlock = unlock - 1
                lock2.image = UIImage(named: "lock")
            }
        }
    }
    
    @objc func image3LongClickGesture(gesture: UILongPressGestureRecognizer) {
        // prepare long tap listener
        if gesture.state == .began {
            lock3.image = UIImage(named: "lock_pressed")
            unlock = unlock + 1
        }
        else if gesture.state == .ended {
            if unlock == 3 {
                SuccesVerified()
            }
            else {
                unlock = unlock - 1
                lock3.image = UIImage(named: "lock")
            }
        }
    }
    
    @objc func SuccesVerified() {
        DismisParentalGate(success: true)
    }
    
    @objc func DismisParentalGate(success: Bool) {
        
        self.delegate?.ParentalGateControllerResponse(success: success)
        
        dismiss(animated: true, completion: nil)
    }
}
