//
//  GameSelectController.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2018 Kids' Library Apps. All rights reserved.
//

import UIKit
import AudioToolbox

class GameSelectController: UIViewController {
    @IBOutlet weak var SelectImageName: UILabel!
    @IBOutlet weak var SelectImage1: UIImageView!
    @IBOutlet weak var SelectImage2: UIImageView!
    @IBOutlet weak var SelectImage3: UIImageView!
    
    // delay config
    var delayTime: UInt32 = 1000
    
    // end level
    var nextLevel: Bool = false
    var nextWindow: Bool = false
    
    // objects
    var objects = [Objects]()
    var player = Player()
    var playerSelect = Player()
    var settings = SettingsApp()
    
    // current main object
    var mainObject: Int = 0
    var oldObject: Int = -1
    var mainObjectName: String = ""
    var oldObjectName: String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // hide back button text
        let backButton = UIBarButtonItem(image: UIImage(named: "backImg"), style: .plain, target: self, action: #selector(backPress))
        backButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = backButton
        
        // enable notification
        NotificationCenter.default.addObserver(self, selector: #selector(applyCurrentSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    
        // prepare data for Activity
        GenerateGameContent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // apply current settings
        applyCurrentSettings()
        
        // create tab navigation buttons
        let help = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(openHelpView))
        let next = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(tryOpenNextView))
        
        navigationItem.setRightBarButtonItems([next, help], animated: true)
        // set navigation visible
        self.navigationController?.isNavigationBarHidden = false
        
        // complete data for Activity
        CompleteGameContent()
    }
    
    @objc func backPress() {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @objc func applyCurrentSettings() {
        
        if nextWindow {
            return
        }
        
        // read current settings
        settings.readSettings()

        // set navigation bar  title
        navigationItem.title = ProjectText.categories[0].localized(lang: settings.app_language!)
        
        // complete data for Activity
        CompleteGameContent()
    }
    
    @objc func openHelpView() {
        let pageItemController = self.storyboard?.instantiateViewController(withIdentifier: "ObjectViewController") as! ObjectViewController
        
        pageItemController.itemIndex = 0
        
        pageItemController.lableName = objects[mainObject].Name
        pageItemController.imageName = objects[mainObject].Image
        pageItemController.descriptionName = objects[mainObject].Description
        
        pageItemController.soundName = objects[mainObject].SoundName
        pageItemController.soundItem = objects[mainObject].Sound
        
        self.navigationController?.pushViewController(pageItemController, animated:true)
    }
    
    @objc func tryOpenNextView() {
        OpenNextView()
    }
    
    private func GenerateGameContent() {
        // get 1 object
        var obj = GetRandomObject()
        objects.append(obj)
        
        // get 2 object
        obj = GetRandomObject()
        
        while (obj.Name == objects[0].Name) {
            do {
                usleep(delayTime)
                
                // Do some stuff
                obj = GetRandomObject();
            }
        }
        
        objects.append(obj)
        
        // get 3 object
        obj = GetRandomObject()
        
        while (obj.Name == objects[0].Name ||
                obj.Name == objects[1].Name) {
            do {
                usleep(delayTime)
                
                // Do some stuff
                obj = GetRandomObject();
            }
        }
        
        objects.append(obj)

        // get main object
        mainObject = ApplicationWork.GetRandomNumber(min: 1, max: 3) - 1;
        
        while(!oldObjectName.isEmpty && oldObjectName.lowercased() == objects[mainObject].Name.lowercased() ||
            oldObject == mainObject) {
            do {
                usleep(delayTime)
                // Do some stuff
                mainObject = ApplicationWork.GetRandomNumber(min: 1, max: 3) - 1;
            }
        }
        
        mainObjectName = objects[mainObject].Name
    }
    
    private func GetRandomObject() -> Objects {
        let objects = Objects()
        objects.GetRandomObject();
        return objects;
    }
    
    private func CompleteGameContent() {
        // error input data
        if(mainObject < 0 || mainObject > 2) {
            finish()
            return;
        }
        
        // set text as main text
        SelectImageName.font = SelectImageName.font.withSize(CGFloat(settings.pref_card_name_size!))
        
        SelectImageName.text = objects[mainObject].Name.localized(lang: settings.app_language!)
        
        if(settings.cards_name_upper!) {
            SelectImageName.text = SelectImageName.text?.uppercased()
        }
        
        switch settings.pref_cards_name_color! {
        case "gray":
            SelectImageName.textColor = UIColor.gray
        case "blue":
            SelectImageName.textColor = UIColor.blue
        case "red":
            SelectImageName.textColor = UIColor.red
        default:
            SelectImageName.textColor = UIColor.black
        }
        
        // set all images

        // prepare 1 image
        SelectImage1.image = UIImage(named: objects[0].Image)
        SelectImage1.isUserInteractionEnabled = true
        // create 1 tap listener
        let clickTapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.SelectedVariant1) )
        clickTapGesture1.numberOfTapsRequired = 1
        SelectImage1.addGestureRecognizer(clickTapGesture1)
        
        // prepare 2 image
        SelectImage2.image = UIImage(named: objects[1].Image)
        SelectImage2.isUserInteractionEnabled = true
        let clickTapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.SelectedVariant2) )
        clickTapGesture2.numberOfTapsRequired = 1
        SelectImage2.addGestureRecognizer(clickTapGesture2)
        
        // prepare 3 image
        SelectImage3.image = UIImage(named: objects[2].Image)
        SelectImage3.isUserInteractionEnabled = true
        let clickTapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.SelectedVariant3) )
        clickTapGesture3.numberOfTapsRequired = 1
        SelectImage3.addGestureRecognizer(clickTapGesture3)

        
        // play main object name
        ApplicationWork.delayWithSeconds(0.8) {
            self.player.Play(soundName: self.objects[self.mainObject].SoundName, loop: false, localized: true)
        }
    }

    @objc func SelectedVariant1(gesture: UIGestureRecognizer) {
        // prepare 1 tap listener
        if(mainObject == 0) {
            SelectedCorrectVariant(SelectImage2, SelectImage3, &nextLevel);
        }
        else {
            SelectedWrongVariant(SelectImage1);
        }
    }
    
    @objc func SelectedVariant2(gesture: UIGestureRecognizer) {
        // prepare 1 tap listener
        if(mainObject == 1) {
            SelectedCorrectVariant(SelectImage1, SelectImage3, &nextLevel);
        }
        else {
            SelectedWrongVariant(SelectImage2);
        }
    }
    
    @objc func SelectedVariant3(gesture: UIGestureRecognizer) {
        // prepare 1 tap listener
        if(mainObject == 2) {
            SelectedCorrectVariant(SelectImage1, SelectImage2, &nextLevel);
        }
        else {
            SelectedWrongVariant(SelectImage3);
        }
    }

    func SelectedCorrectVariant(_ view1: UIView, _ view2: UIView, _ nextLevel: inout Bool) {
        if nextLevel == true {
            OpenNextView()
        }
        else {
            // play correct variant sound
            playerSelect.Play(soundName: "game_sound_correct", loop: false, localized: false)
            
            // animate main view
            UIView.animate(withDuration: 1, animations: {
                view1.isHidden = true
            })
            
            UIView.animate(withDuration: 1, animations: {
                view2.isHidden = true
            })
            
            // go to next level
            nextLevel = true
        }
    }

    private func SelectedWrongVariant(_ view: UIImageView) {
        //view.image = view.image?.noir // now is very slow in virtual device
        VibrateShort()
        playerSelect.Play(soundName: "game_sound_wrong", loop: false, localized: false)
    }

    func OpenNextView() {
        finish()
        nextWindow = true
        
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "GameSelectController") as? GameSelectController
        vc?.oldObjectName = mainObjectName
        vc?.oldObject = mainObject
        self.navigationController?.pushViewController(vc!, animated:true)
    }

    func VibrateShort() {
        // Vibrate for 150 milliseconds
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    func finish() {
        //self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
}
