//
//  Objects.swift
//  Animals
//
//  Created by Vitali Carajileasco on 5/7/18.
//  Copyright © 2018 Kids' Library Apps. All rights reserved.
//

import Foundation

class Objects {

    // Object parameters
    public var Name: String
    public var Description: String
    
    public var Image: String
    
    public var SoundName: String
    public var Sound: String
    
    init() {
        self.Name = ""
        self.Description = ""
        self.Image = ""
        self.SoundName = ""
        self.Sound = ""
    }
    
    public func GetRandomObject() {
        let category = GetRandomCategory()
        let object = GetRandomObjectFromCategory(category)
    
        Name = GetObjectName(category, object)
        Description = GetObjectDescription(category, object)
    
        Image = GetObjectImage(category, object)
    
        Sound = GetObjectSound(category, object)
        SoundName = GetObjectSoundName(category, object)
    }
    
    private func GetRandomCategory() -> Int {
        return ApplicationWork.GetRandomNumber(min: 1, max: ProjectText.subCategories.count - 1)
    }
    
    private func GetRandomObjectFromCategory(_ category: Int) -> Int {
        var ret = -1;
    
        if(category >= 0) {
            ret = ApplicationWork.GetRandomNumber(min: 1, max: ProjectImages.itemImages[category].count) - 1
        }
    
        return ret;
    }
    
    private func GetObjectName(_ category: Int, _ object: Int) -> String {
        var ret: String = ""
        
        if(category >= 0 && object >= 0) {
            ret = ProjectText.names[category][object]
        }
        
        return ret;
    }
    
    private func GetObjectDescription(_ category: Int, _ object: Int) -> String {
        var ret: String = ""
        
        if(category >= 0 && object >= 0) {
            ret = ProjectText.description[category][object]
        }
        
        return ret;
    }
    
    private func GetObjectImage(_ category: Int, _ object: Int) -> String {
        var ret: String = ""
        
        if(category >= 0 && object >= 0) {
            ret = ProjectImages.itemImages[category][object];
        }
        
        return ret;
    }
    
    private func GetObjectSoundName(_ category: Int, _ object: Int) -> String {
        var ret: String = ""
        
        if(category >= 0 && object >= 0) {
            ret = ProjectSound.soundNames[category][object];
        }
        
        return ret;
    }
    
    private func GetObjectSound(_ category: Int, _ object: Int) -> String {
    var ret: String = ""
    
        if(category >= 0 && object >= 0) {
            if(ProjectSound.soundObjects[category].count > object) {
                ret = ProjectSound.soundObjects[category][object];
            }
        }
        
        return ret;
    }
    
}
