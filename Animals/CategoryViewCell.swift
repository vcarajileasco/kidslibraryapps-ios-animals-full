	//
//  TableViewCell.swift
//  Music
//
//  Created by Admin on 04.10.17.
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import UIKit

class CategoryViewCell: UITableViewCell {

    // components fom view
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
