//
//  TableViewController.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import UIKit

class CategoriesViewController: UITableViewController {
    // toolbox
    @IBOutlet var categoriesTableView: UITableView!
    
    // objects
    var player = Player()
    var settings = SettingsApp()
    
    // variables
    var indexID: Int = 0
    
    
    // basic functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        // enable notification
        NotificationCenter.default.addObserver(self, selector: #selector(applyCurrentSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // apply current settings
        applyCurrentSettings()
       
        // set navigation visible
        self.navigationController?.isNavigationBarHidden = false
    }

    @objc func applyCurrentSettings() {
        // read current settings
        self.settings.readSettings()
       
        // set navigation bar  title
        navigationItem.title = ProjectText.categories[0].localized(lang: settings.app_language!)
        
        // reload table view content
        self.categoriesTableView?.reloadData()
    }
    
    func openSlider(index: Int) {
        // open slider with index
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "SliderViewController") as? SliderViewController
        
        vc?.subcategoryID = index
        self.navigationController?.pushViewController(vc!, animated:true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ProjectText.subCategories.count - 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryViewCell

        // Configure the cell...
        cell.cellLabel?.text = ProjectText.subCategories[indexPath.row + 1].localized(lang: settings.app_language!)
        cell.cellImage?.image = UIImage(named: ProjectImages.subCategoriesImages[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
    
        openSlider(index: index + 1)
    }

}
