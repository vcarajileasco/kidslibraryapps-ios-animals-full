//
//  ProjectSound.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import Foundation

class ProjectSound {
    public static let appSound = "music_app";
    
    public static let soundNames =
        [
            // how to use
            [
                "user_guide_name"
            ],
            // home animals
            [
                "cat_1_1_name_1",
                "cat_1_1_name_2",
                "cat_1_1_name_3",
                "cat_1_1_name_4",
                "cat_1_1_name_5",
                "cat_1_1_name_6",
                "cat_1_1_name_7",
                "cat_1_1_name_8",
                "cat_1_1_name_9",
                "cat_1_1_name_10",
                "cat_1_1_name_11",
                "cat_1_1_name_12",
                "cat_1_1_name_13",
                "cat_1_1_name_14",
                "cat_1_1_name_15",
                "cat_1_1_name_16",
                "cat_1_1_name_17",
                "cat_1_1_name_18",
                "cat_1_1_name_19",
                "cat_1_1_name_20",
                "cat_1_1_name_21",
            ],
            // forest animals
            [
                "cat_1_2_name_1",
                "cat_1_2_name_2",
                "cat_1_2_name_3",
                "cat_1_2_name_4",
                "cat_1_2_name_5",
                "cat_1_2_name_6",
                "cat_1_2_name_7",
                "cat_1_2_name_8",
                "cat_1_2_name_9",
                "cat_1_2_name_10",
                "cat_1_2_name_11",
                "cat_1_2_name_12",
                "cat_1_2_name_13",
                "cat_1_2_name_14",
            ],
            // polar animals
            [
                "cat_1_3_name_1",
                "cat_1_3_name_2",
                "cat_1_3_name_3",
                "cat_1_3_name_4",
                "cat_1_3_name_5",
                "cat_1_3_name_6",
                "cat_1_3_name_7",
                "cat_1_3_name_8",
                "cat_1_3_name_9",
                "cat_1_3_name_10",
                "cat_1_3_name_11",
                "cat_1_3_name_12",
                "cat_1_3_name_13",
                "cat_1_3_name_14",
                "cat_1_3_name_15",
                "cat_1_3_name_16",
                "cat_1_3_name_17",
            ],
            // savannas animals
            [
                "cat_1_4_name_1",
                "cat_1_4_name_2",
                "cat_1_4_name_3",
                "cat_1_4_name_4",
                "cat_1_4_name_5",
                "cat_1_4_name_6",
                "cat_1_4_name_7",
                "cat_1_4_name_8",
                "cat_1_4_name_9",
                "cat_1_4_name_10",
                "cat_1_4_name_11",
            ],
            // tropic animals
            [
                "cat_1_5_name_1",
                "cat_1_5_name_2",
                "cat_1_5_name_3",
                "cat_1_5_name_4",
                "cat_1_5_name_5",
                "cat_1_5_name_6",
                "cat_1_5_name_7",
                "cat_1_5_name_8",
                "cat_1_5_name_9",
                "cat_1_5_name_10",
                "cat_1_5_name_11",
                "cat_1_5_name_12",
                "cat_1_5_name_13",
                "cat_1_5_name_14",
                "cat_1_5_name_15",
            ],
            // australia animals
            [
                "cat_1_6_name_1",
                "cat_1_6_name_2",
                "cat_1_6_name_3",
                "cat_1_6_name_4",
                "cat_1_6_name_5",
                "cat_1_6_name_6",
                "cat_1_6_name_7",
                "cat_1_6_name_8",
                "cat_1_6_name_9",
            ],
        ]
    
    public static let soundObjects =
        [
            // how to use
            [
                "user_guide_sound",
            ],
            // home animals
            [
                "cat_1_1_object_1",
                "cat_1_1_object_2",
                "cat_1_1_object_3",
                "cat_1_1_object_4",
                "cat_1_1_object_5",
                "cat_1_1_object_6",
                "cat_1_1_object_7",
                "cat_1_1_object_8",
                "cat_1_1_object_9",
                "cat_1_1_object_10",
                "cat_1_1_object_11",
                "cat_1_1_object_12",
                "cat_1_1_object_13",
                "cat_1_1_object_14",
                "cat_1_1_object_15",
                "cat_1_1_object_16",
                "cat_1_1_object_17",
                "cat_1_1_object_18",
                "cat_1_1_object_19",
                "cat_1_1_object_20",
                "cat_1_1_object_21",
            ],
            // forest animals
            [
                "cat_1_2_object_1",
                "cat_1_2_object_2",
                "cat_1_2_object_3",
                "cat_1_2_object_4",
                "cat_1_2_object_5",
                "cat_1_2_object_6",
                "cat_1_2_object_7",
                "cat_1_2_object_8",
                "cat_1_2_object_9",
                "cat_1_2_object_10",
                "cat_1_2_object_11",
                "cat_1_2_object_12",
                "cat_1_2_object_13",
                "cat_1_2_object_14",
            ],
            // polar animals
            [
                "cat_1_3_object_1",
                "cat_1_3_object_2",
                "cat_1_3_object_3",
                "cat_1_3_object_4",
                "cat_1_3_object_5",
                "cat_1_3_object_6",
                "cat_1_3_object_7",
                "cat_1_3_object_8",
                "cat_1_3_object_9",
                "cat_1_3_object_10",
                "cat_1_3_object_11",
                "cat_1_3_object_12",
                "cat_1_3_object_13",
                "cat_1_3_object_14",
                "cat_1_3_object_15",
                "cat_1_3_object_16",
                "cat_1_3_object_17",
            ],
            // savannas animals
            [
                "cat_1_4_object_1",
                "cat_1_4_object_2",
                "cat_1_4_object_3",
                "cat_1_4_object_4",
                "cat_1_4_object_5",
                "cat_1_4_object_6",
                "cat_1_4_object_7",
                "cat_1_4_object_8",
                "cat_1_4_object_9",
                "cat_1_4_object_10",
                "cat_1_4_object_11",
            ],
            // tropic animals
            [
                "cat_1_5_object_1",
                "cat_1_5_object_2",
                "cat_1_5_object_3",
                "cat_1_5_object_4",
                "cat_1_5_object_5",
                "cat_1_5_object_6",
                "cat_1_5_object_7",
                "cat_1_5_object_8",
                "cat_1_5_object_9",
                "silent",
                "cat_1_5_object_11",
                "cat_1_5_object_12",
                "cat_1_5_object_13",
                "cat_1_5_object_14",
                "cat_1_5_object_15",
            ],
            // australia animals
            [
                "cat_1_6_object_1",
                "cat_1_6_object_2",
                "cat_1_6_object_3",
                "silent",
                "cat_1_6_object_5",
                "cat_1_6_object_6",
                "cat_1_6_object_7",
                "cat_1_6_object_8",
                "cat_1_6_object_9",
            ],
        ]
    
}
