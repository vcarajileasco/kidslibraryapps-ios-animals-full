//
//  ProjectText.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import Foundation

class ProjectText {
    public static let app_main_name = "app_main_name"
    
    public static let apps_id =
    [
        "1295265163",   // app store
        "1385172532",   // free
        "1295265164",   // full
    ]
    
    public static let appUrl =
        [
            "https://itunes.apple.com/app/kids-library-animals-full/id1295265164"
        ]
    
    public static let tabBar =
        [
            "nav_main",
            "nav_game",
        ]
    
    public static let navigation =
        [
            "nav_share",
            "nav_rate",
            "nav_privacy_policy",
            "nav_all_apps",
            "nav_help",
        ]
    
    public static let share =
        [
            "nav_share",
            "nav_share_message",
        ]
    
    public static let language =
        [
            "lang_ru",
            "lang_en",
            "lang_ro",
        ]
    
    public static let questionLang =
        [
            "question_lang_title",
            "question_lang_mess",
        ]
        
    
    public static let parentsGate =
        [
            "parent_gate_name",
            "parent_gate_text",
        ]
    
    public static let rateApp =
        [
            "rate_title",
            "rate_message",
            "rate_yes",
            "rate_later",
            "rate_no",
        ]
    
    public static let games =
        [
            "game_1_name",
        ]
    
    
    public static let categories =
        [
            "category_1_name",
        ]
    
    public static let subCategories =
        [
            // how to use
            "category_1_name",
            
            // objects categories
            "sub_objects_1",
            "sub_objects_2",
            "sub_objects_3",
            "sub_objects_4",
            "sub_objects_5",
            "sub_objects_6",
        ]
    
    public static let names =
        [
            // how to use
            [
                "category_1_name",
            ],
            // home animals
            [
                "sub_object_1_1",
                "sub_object_1_2",
                "sub_object_1_3",
                "sub_object_1_4",
                "sub_object_1_5",
                "sub_object_1_6",
                "sub_object_1_7",
                "sub_object_1_8",
                "sub_object_1_9",
                "sub_object_1_10",
                "sub_object_1_11",
                "sub_object_1_12",
                "sub_object_1_13",
                "sub_object_1_14",
                "sub_object_1_15",
                "sub_object_1_16",
                "sub_object_1_17",
                "sub_object_1_18",
                "sub_object_1_19",
                "sub_object_1_20",
                "sub_object_1_21",
            ],
            // forest animals
            [
                "sub_object_2_1",
                "sub_object_2_2",
                "sub_object_2_3",
                "sub_object_2_4",
                "sub_object_2_5",
                "sub_object_2_6",
                "sub_object_2_7",
                "sub_object_2_8",
                "sub_object_2_9",
                "sub_object_2_10",
                "sub_object_2_11",
                "sub_object_2_12",
                "sub_object_2_13",
                "sub_object_2_14",
            ],
            // polar animals
            [
                "sub_object_3_1",
                "sub_object_3_2",
                "sub_object_3_3",
                "sub_object_3_4",
                "sub_object_3_5",
                "sub_object_3_6",
                "sub_object_3_7",
                "sub_object_3_8",
                "sub_object_3_9",
                "sub_object_3_10",
                "sub_object_3_11",
                "sub_object_3_12",
                "sub_object_3_13",
                "sub_object_3_14",
                "sub_object_3_15",
                "sub_object_3_16",
                "sub_object_3_17",
            ],
            // savannas animals
            [
                "sub_object_4_1",
                "sub_object_4_2",
                "sub_object_4_3",
                "sub_object_4_4",
                "sub_object_4_5",
                "sub_object_4_6",
                "sub_object_4_7",
                "sub_object_4_8",
                "sub_object_4_9",
                "sub_object_4_10",
                "sub_object_4_11",
            ],
            // tropic animals
            [
                "sub_object_5_1",
                "sub_object_5_2",
                "sub_object_5_3",
                "sub_object_5_4",
                "sub_object_5_5",
                "sub_object_5_6",
                "sub_object_5_7",
                "sub_object_5_8",
                "sub_object_5_9",
                "sub_object_5_10",
                "sub_object_5_11",
                "sub_object_5_12",
                "sub_object_5_13",
                "sub_object_5_14",
                "sub_object_5_15",
            ],
            // australia animals
            [
                "sub_object_6_1",
                "sub_object_6_2",
                "sub_object_6_3",
                "sub_object_6_4",
                "sub_object_6_5",
                "sub_object_6_6",
                "sub_object_6_7",
                "sub_object_6_8",
                "sub_object_6_9",
            ],
        ]
    
    public static let description =
        [
            // how to use
            [
                "user_guide_information",
            ],
            // home animals
            [
                "sub_object_1_desc_1",
                "sub_object_1_desc_2",
                "sub_object_1_desc_3",
                "sub_object_1_desc_4",
                "sub_object_1_desc_5",
                "sub_object_1_desc_6",
                "sub_object_1_desc_7",
                "sub_object_1_desc_8",
                "sub_object_1_desc_9",
                "sub_object_1_desc_10",
                "sub_object_1_desc_11",
                "sub_object_1_desc_12",
                "sub_object_1_desc_13",
                "sub_object_1_desc_14",
                "sub_object_1_desc_15",
                "sub_object_1_desc_16",
                "sub_object_1_desc_17",
                "sub_object_1_desc_18",
                "sub_object_1_desc_19",
                "sub_object_1_desc_20",
                "sub_object_1_desc_21",
            ],
            // forest animals
            [
                "sub_object_2_desc_1",
                "sub_object_2_desc_2",
                "sub_object_2_desc_3",
                "sub_object_2_desc_4",
                "sub_object_2_desc_5",
                "sub_object_2_desc_6",
                "sub_object_2_desc_7",
                "sub_object_2_desc_8",
                "sub_object_2_desc_9",
                "sub_object_2_desc_10",
                "sub_object_2_desc_11",
                "sub_object_2_desc_12",
                "sub_object_2_desc_13",
                "sub_object_2_desc_14",
            ],
            // polar animals
            [
                "sub_object_3_desc_1",
                "sub_object_3_desc_2",
                "sub_object_3_desc_3",
                "sub_object_3_desc_4",
                "sub_object_3_desc_5",
                "sub_object_3_desc_6",
                "sub_object_3_desc_7",
                "sub_object_3_desc_8",
                "sub_object_3_desc_9",
                "sub_object_3_desc_10",
                "sub_object_3_desc_11",
                "sub_object_3_desc_12",
                "sub_object_3_desc_13",
                "sub_object_3_desc_14",
                "sub_object_3_desc_15",
                "sub_object_3_desc_16",
                "sub_object_3_desc_17",
            ],
            // savannas animals
            [
                "sub_object_4_desc_1",
                "sub_object_4_desc_2",
                "sub_object_4_desc_3",
                "sub_object_4_desc_4",
                "sub_object_4_desc_5",
                "sub_object_4_desc_6",
                "sub_object_4_desc_7",
                "sub_object_4_desc_8",
                "sub_object_4_desc_9",
                "sub_object_4_desc_10",
                "sub_object_4_desc_11",
            ],
            // tropic animals
            [
                "sub_object_5_desc_1",
                "sub_object_5_desc_2",
                "sub_object_5_desc_3",
                "sub_object_5_desc_4",
                "sub_object_5_desc_5",
                "sub_object_5_desc_6",
                "sub_object_5_desc_7",
                "sub_object_5_desc_8",
                "sub_object_5_desc_9",
                "sub_object_5_desc_10",
                "sub_object_5_desc_11",
                "sub_object_5_desc_12",
                "sub_object_5_desc_13",
                "sub_object_5_desc_14",
                "sub_object_5_desc_15",
            ],
            // australia animals
            [
                "sub_object_6_desc_1",
                "sub_object_6_desc_2",
                "sub_object_6_desc_3",
                "sub_object_6_desc_4",
                "sub_object_6_desc_5",
                "sub_object_6_desc_6",
                "sub_object_6_desc_7",
                "sub_object_6_desc_8",
                "sub_object_6_desc_9",
                ],
            ]
    
}
