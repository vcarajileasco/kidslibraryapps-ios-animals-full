//
//  ProjectImages.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import Foundation

class ProjectImages {
    public static let subCategoriesImages =
        // animals
        [
            "cat_1_1",
            "cat_1_2",
            "cat_1_3",
            "cat_1_4",
            "cat_1_5",
            "cat_1_6"
        ]
    
    public static let games =
        // animals
        [
            "game_select",
    ]
    
    public static let itemImages =
        [
            // how to use
            [
                "about_img",
            ],
            // home animals
            [
                "cat_1_1_obj_1",
                "cat_1_1_obj_2",
                "cat_1_1_obj_3",
                "cat_1_1_obj_4",
                "cat_1_1_obj_5",
                "cat_1_1_obj_6",
                "cat_1_1_obj_7",
                "cat_1_1_obj_8",
                "cat_1_1_obj_9",
                "cat_1_1_obj_10",
                "cat_1_1_obj_11",
                "cat_1_1_obj_12",
                "cat_1_1_obj_13",
                "cat_1_1_obj_14",
                "cat_1_1_obj_15",
                "cat_1_1_obj_16",
                "cat_1_1_obj_17",
                "cat_1_1_obj_18",
                "cat_1_1_obj_19",
                "cat_1_1_obj_20",
                "cat_1_1_obj_21",
            ],
            // forest animals
            [
                "cat_1_2_obj_1",
                "cat_1_2_obj_2",
                "cat_1_2_obj_3",
                "cat_1_2_obj_4",
                "cat_1_2_obj_5",
                "cat_1_2_obj_6",
                "cat_1_2_obj_7",
                "cat_1_2_obj_8",
                "cat_1_2_obj_9",
                "cat_1_2_obj_10",
                "cat_1_2_obj_11",
                "cat_1_2_obj_12",
                "cat_1_2_obj_13",
                "cat_1_2_obj_14",
            ],
            // polar animals
            [
                "cat_1_3_obj_1",
                "cat_1_3_obj_2",
                "cat_1_3_obj_3",
                "cat_1_3_obj_4",
                "cat_1_3_obj_5",
                "cat_1_3_obj_6",
                "cat_1_3_obj_7",
                "cat_1_3_obj_8",
                "cat_1_3_obj_9",
                "cat_1_3_obj_10",
                "cat_1_3_obj_11",
                "cat_1_3_obj_12",
                "cat_1_3_obj_13",
                "cat_1_3_obj_14",
                "cat_1_3_obj_15",
                "cat_1_3_obj_16",
                "cat_1_3_obj_17",
            ],
            // savannas animals
            [
                "cat_1_4_obj_1",
                "cat_1_4_obj_2",
                "cat_1_4_obj_3",
                "cat_1_4_obj_4",
                "cat_1_4_obj_5",
                "cat_1_4_obj_6",
                "cat_1_4_obj_7",
                "cat_1_4_obj_8",
                "cat_1_4_obj_9",
                "cat_1_4_obj_10",
                "cat_1_4_obj_11",
            ],
            // tropic animals
            [
                "cat_1_5_obj_1",
                "cat_1_5_obj_2",
                "cat_1_5_obj_3",
                "cat_1_5_obj_4",
                "cat_1_5_obj_5",
                "cat_1_5_obj_6",
                "cat_1_5_obj_7",
                "cat_1_5_obj_8",
                "cat_1_5_obj_9",
                "cat_1_5_obj_10",
                "cat_1_5_obj_11",
                "cat_1_5_obj_12",
                "cat_1_5_obj_13",
                "cat_1_5_obj_14",
                "cat_1_5_obj_15",
            ],
            // australia animals
            [
                "cat_1_6_obj_1",
                "cat_1_6_obj_2",
                "cat_1_6_obj_3",
                "cat_1_6_obj_4",
                "cat_1_6_obj_5",
                "cat_1_6_obj_6",
                "cat_1_6_obj_7",
                "cat_1_6_obj_8",
                "cat_1_6_obj_9",
            ],
        ]
}
