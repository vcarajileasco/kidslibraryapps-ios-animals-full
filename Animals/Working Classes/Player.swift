//
//  Player.swift
//  Kids' Library Apps
//
//  Created by Admin
//  Copyright © 2017 Kids' Library Apps. All rights reserved.
//

import UIKit
import AVFoundation

class Player: NSObject {
    private var avPlayer: AVAudioPlayer?
    private var settings = SettingsApp()

    public func Play(soundName: String, loop: Bool, localized: Bool) {
        // check soundName file
        if soundName.isEmpty {
            return
        }
        
        settings.readSettings()
    
        // check current AVPlayer state
        if self.avPlayer != nil {
            if self.avPlayer!.isPlaying {
                return
            }
        }

        // try to stop AVPlayer
        Stop();
        
        // get sound file from Resources
        var audioPath: String?
        
        if localized {
            audioPath = Bundle.main.path(forResource: soundName, ofType: "mp3",
                                         inDirectory: settings.app_sounddir,
                                         forLocalization: settings.app_language)
        }
        else {
            audioPath = Bundle.main.path(forResource: soundName, ofType: "mp3")
        }
        
        if audioPath != nil {
            // try to play sound file
            do {
                self.avPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!) as URL)
                
                if loop {
                    self.avPlayer?.numberOfLoops = 1000
                }
                else {
                    self.avPlayer?.numberOfLoops = 0
                }
                
                self.avPlayer?.prepareToPlay()
                self.avPlayer?.play()
            }
            catch {
                print("Error getting the audio file")
            }
        }
    }
    
    public func Stop(){
        // try to stop sound file
        self.avPlayer?.stop()
    }
}
