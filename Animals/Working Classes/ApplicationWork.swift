//
//  ApplicationWork.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import UIKit
import StoreKit

class ApplicationWork: NSObject {
    public static func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    public static func sleepScreenEnable(enable: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !enable
    }
    
    public static func GetRandomNumber(min: Int, max: Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    public static func openAppStore() {
        /* First create a URL, then check whether there is an installed app that can open it on the device. */
        let appId: String = ProjectText.apps_id[0]
        if let url = URL(string: "https://itunes.apple.com/us/developer/vitali-carajileasco/id" + appId + "?mt=8"),
            UIApplication.shared.canOpenURL(url){
            // Attempt to open the URL.
            UIApplication.shared.openURL(url)
        }
    }
    
    public static func requestReviewManually() {
        // Note: Replace the XXXXXXXXXX below with the App Store ID for your app
        //       You can find the App Store ID in your app's product URL
        let appId: String = ProjectText.apps_id[2]
        guard let writeReviewURL = URL(string: "https://itunes.apple.com/app/id" + appId + "?action=write-review")
            else {
                fatalError("Expected a valid URL")
        }
        
        UIApplication.shared.openURL(writeReviewURL)
    }
    
    public static func buyFullApp() {
        // Note: Replace the XXXXXXXXXX below with the App Store ID for your app
        //       You can find the App Store ID in your app's product URL
        let appId: String = ProjectText.apps_id[2]
        guard let writeReviewURL = URL(string: "https://itunes.apple.com/app/id" + appId + "?")
            else {
                fatalError("Expected a valid URL")
        }
        
        UIApplication.shared.openURL(writeReviewURL)
    }
    
    public static func openUrl(url: String) {
        if let openUrl = URL(string: url) {
            UIApplication.shared.openURL(openUrl)
        }
    }

    func openViewWithNavigationBar() {
        //let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //let sliderViewController = storyBoard.instantiateViewController(withIdentifier: "TableViewController") as! TableViewController
        //self.navigationController?.pushViewController(sliderViewController, animated: true)
    }
    
    func openViewWithoutNavigationBar() {
        OperationQueue.main.addOperation {
            //let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //let sliderViewController = storyBoard.instantiateViewController(withIdentifier: "TableViewController") as! TableViewController
            //self.present(sliderViewController, animated: true, completion: nil)
        }
    }
    
    
}
