//
//  DefaultsApp.swift
//  ABC Full
//
//  Created by Vitali Carajileasco on 7/24/18.
//  Copyright © 2018 Kids' Library. All rights reserved.
//

import Foundation

class DefaultsApp {
    static let MaxValue: Int = 6
    
    public static func store(value: Int) {
        
        if value <= MaxValue {
            let defaults = UserDefaults.standard
            defaults.set(value, forKey: "rateApp")
            defaults.synchronize()
        }
    }
    
    public static func get() -> Int {
        let defaults = UserDefaults.standard
        
        let rateApp: Int = defaults.integer(forKey: "rateApp")
        return rateApp
    }
}
