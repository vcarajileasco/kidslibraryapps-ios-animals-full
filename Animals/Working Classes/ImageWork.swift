//
//  ImageWork.swift
//  Animals
//
//  Created by Vitali Carajileasco on 5/7/18.
//  Copyright © 2018 Kids' Library Apps. All rights reserved.
//

import Foundation
import UIKit

public class ImageWork {
    
    public static func MakeWhiteAndBlack(_ imageView: UIImageView) {
        imageView.layer.masksToBounds = true
        imageView.tintColor = .black
    }
    
    public static func MakeRoundedCorner(_ imageView: UIImageView) {
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
    }

}
