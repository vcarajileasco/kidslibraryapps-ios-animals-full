//
//  SettingsApp.swift
//  Music
//
//  Created by Admin on 04.10.17.
//  Copyright © 2017 Kids' Library. All rights reserved.
//

import Foundation

class SettingsApp {
    // sound
    var sound_music: Bool?
    var sound_names_cards: Bool?
    var sound_cards: Bool?
    var sound_slide_show: Bool?
    
    // slideshow
    var slide_show_speed: Int?
    
    // cards
    var cards_description_show: Bool?
    var cards_name_upper: Bool?
    var pref_card_name_size: Int?
    var pref_card_descip_size: Int?
    
    // colors
    var pref_cards_name_color: String?
    
    // languages
    var app_language: String?
    var app_sounddir: String?
    
    var first_run: Bool?
    
    // read current settings
    func readSettings() {
        let settings = UserDefaults.standard
        
        // sound
        self.sound_music = settings.bool(forKey: "sound_music")
        self.sound_names_cards = settings.bool(forKey: "sound_names_cards")
        self.sound_cards = settings.bool(forKey: "sound_cards")
        self.sound_slide_show = settings.bool(forKey: "sound_slide_show")
        
        // slideshow
        self.slide_show_speed = settings.integer(forKey: "slide_show_speed")
        
        // cards
        self.cards_description_show = settings.bool(forKey: "cards_description_show")
        self.cards_name_upper = settings.bool(forKey: "cards_name_upper")
        self.pref_card_name_size = settings.integer(forKey: "pref_card_name_size")
        self.pref_card_descip_size = settings.integer(forKey: "pref_card_descip_size")
        
        // colors
        self.pref_cards_name_color = settings.string(forKey: "pref_cards_name_color")
        
        // languages
        self.app_language = settings.string(forKey: "app_language")
        self.app_sounddir = self.app_language! + ".lproj"
        
        self.first_run = settings.bool(forKey: "first_run")
    }
    
    func setDefault () {
        var preferredLanguage = Locale.current.languageCode
        
        if(!AppContainLanguage(lang: preferredLanguage!)) {
            preferredLanguage = "en"
        }
        
        UserDefaults.standard.register(defaults:
            [
                // sound
                "sound_music" : true,
                "sound_names_cards" : true,
                "sound_cards" : true,
                "sound_slide_show" : true,
                
                // slideshow
                "slide_show_speed" : 5,
                
                // cards
                "cards_description_show" : true,
                "cards_name_upper" : true,
                "pref_card_name_size" : 30,
                "pref_card_descip_size" : 20,
                
                // colors
                "pref_cards_name_color" : "red",
                
                // languages
                "app_language" : String(preferredLanguage!),
                
                "first_run" : false
            ]
        )
        
        UserDefaults.standard.synchronize()
    }
    
    func AppContainLanguage(lang: String) -> Bool {
        if(lang.isEmpty) {
            return false;
        }
        
        switch (lang) {
            case "ru":
                return true;
            case "ro":
                return true;
            case "en":
                return true;
            default:
                return false;
        }
    }
    
    func setLanguage(lang: String) {
        let settings = UserDefaults.standard
        
        self.app_language = lang
        
        settings.set(app_language, forKey: "app_language")
        settings.synchronize()
    }
    
    func setFirstRun(state: Bool) {
        let settings = UserDefaults.standard
        
        self.first_run = state
        
        settings.set(first_run, forKey: "first_run")
        settings.synchronize()
    }
    
}
