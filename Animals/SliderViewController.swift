//
//  PageViewController.swift
//  Kids' Library Apps
//
//  Created by Vitali Carajileasco
//  Copyright © 2017 Kids' Library Apps. All rights reserved.
//

import UIKit

class SliderViewController: UIPageViewController, UIPageViewControllerDataSource {
    // custom variables
    var subcategoryID: Int = 0
    var slideshowIsRun: Bool = false
    var slideItemIndex: Int = 0
    
    // custom pomponents
    var slideshow: UIBarButtonItem!
    
    // custom settings
    var settings = SettingsApp()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // apply current settings
        applyCurrentSettings()
        
        // create tab navigation buttons
        slideshow = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(runSlideshow))
        navigationItem.rightBarButtonItem = slideshow
        
        // enable notification
        NotificationCenter.default.addObserver(self, selector: #selector(applyCurrentSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

        // Do any additional setup after loading the view.
        createPageViewController()
        //setupController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // disable sleeping screen
        ApplicationWork.sleepScreenEnable(enable: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // disable sleeping screen
        ApplicationWork.sleepScreenEnable(enable: true)
        
        if self.isMovingFromParentViewController {
            // stop slideshow
            if slideshowIsRun {
                runSlideshow()
            }
        }
    }
    
    @objc func applyCurrentSettings() {
        // read current settings
        settings.readSettings()
        
        navigationItem.title = ProjectText.subCategories[subcategoryID].localized(lang: settings.app_language!)
    }
    
    @objc func runSlideshow() {

        // run slideshow
        if !slideshowIsRun {
            // modify slideshow button
            modifySlideshowControl()
            
            // get current slide
            slideItemIndex = currentControllerIndex()
            
            // show from current item Index to last
            let endIndex = ProjectImages.itemImages[subcategoryID].count
            starSlideShow(endIndex: endIndex)
        }
        // stop slideshow
        else {
            slideItemIndex = ProjectImages.itemImages[subcategoryID].count
        }
    }
    
    func starSlideShow(endIndex: Int) {
        
        if slideItemIndex < endIndex {
            // show new item
            let nextViewController = self.getViewControllerAtIndex(slideItemIndex)
            self.setViewControllers([nextViewController] as? [UIViewController], direction: .forward, animated: true, completion: nil)
            
            // check if sound is enabled in slideshow
            if settings.sound_slide_show! {
                // play sound name after 0.5 second
                ApplicationWork.delayWithSeconds(0.5) {
                    nextViewController?.playObjectName(play: true)
                }
            }
            
            ApplicationWork.delayWithSeconds(Double(settings.slide_show_speed!)) {
                //increase index
                self.slideItemIndex = self.slideItemIndex + 1
                // open next slide
                self.starSlideShow(endIndex: endIndex)
            }
        }
        else {
            modifySlideshowControl()
        }
    }
    
    func modifySlideshowControl() {
        navigationItem.rightBarButtonItem = nil
        
        if slideshowIsRun {
            // show play button
            slideshow = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(runSlideshow))
            navigationItem.rightBarButtonItem = slideshow
        }
        else {
            // show pause button
            slideshow = UIBarButtonItem(barButtonSystemItem: .pause, target: self, action: #selector(runSlideshow))
            navigationItem.rightBarButtonItem = slideshow
        }
        
        slideshowIsRun = !slideshowIsRun
    }

    func createPageViewController() {
        self.dataSource = self
        
        if subcategoryID < 0 {
            return
        }
    
        if ProjectImages.itemImages[subcategoryID].count > 0 {
            
            self.setViewControllers([getViewControllerAtIndex(0)] as? [UIViewController], direction: .forward , animated: false, completion: nil)
        }
    }
    
    func setupController() {
    
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.white
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let itemController: ObjectViewController = viewController as! ObjectViewController
        
        if itemController.itemIndex > 0 {
            return getViewControllerAtIndex(itemController.itemIndex - 1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let itemController = viewController as! ObjectViewController
        
        if itemController.itemIndex + 1 < ProjectImages.itemImages[subcategoryID].count {
            return getViewControllerAtIndex(itemController.itemIndex + 1)
        }
        
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return ProjectImages.itemImages[subcategoryID].count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func currentControllerIndex() -> Int {
        let pageItemController = self.currentController()
        
        if let controller = pageItemController as? ObjectViewController {
            return controller.itemIndex
        }
        
        return +1
    }
    
    func currentController() -> UIViewController? {
        if (self.viewControllers?.count)! > 0 {
            return self.viewControllers![0]
        }
        
        return nil
    }
    
    func getViewControllerAtIndex(_ itemIndex: Int) -> ObjectViewController? {
        if itemIndex < ProjectImages.itemImages[subcategoryID].count {
            let pageItemController = self.storyboard?.instantiateViewController(withIdentifier: "ObjectViewController") as! ObjectViewController
            
            pageItemController.itemIndex = itemIndex

            pageItemController.lableName = ProjectText.names[subcategoryID][itemIndex]
            pageItemController.imageName = ProjectImages.itemImages[subcategoryID][itemIndex]
            pageItemController.descriptionName = ProjectText.description[subcategoryID][itemIndex]
            
            pageItemController.soundName = ProjectSound.soundNames[subcategoryID][itemIndex]
            pageItemController.soundItem = ProjectSound.soundObjects[subcategoryID][itemIndex]
            
            return pageItemController
        }
        
        return nil
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
