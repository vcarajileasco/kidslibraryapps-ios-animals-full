//
//  MainTabBarController.swift
//  ABC Full
//
//  Created by Vitali Carajileasco on 5/25/19.
//  Copyright © 2019 Kids' Library. All rights reserved.
//

import UIKit
import PopMenu
import MessageUI

class MainTabBarController: BaseTabBarController {
    // general items
    var player = Player()
    var settings = SettingsApp()
    
    // menues
    var menuViewController: PopMenuViewController!
    var languageViewController: PopMenuViewController!
    
    // open external apps
    var openShareApp = false
    var openRateApp = false
    var openPrivacyPolicy = false
    var openAppStore = false
    
    
    override func viewDidLoad() {
        self.navigationItem.hidesBackButton = true
        //self.navigationItem.title = "WellDone"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menu"), style: .done, target: self, action: #selector(self.presentMenu))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "ic_action_language"), style: .done, target: self, action: #selector(self.presentLanguage))
        
        // update default settings
        settings.setDefault ()
        
        // enable notification
        NotificationCenter.default.addObserver(self, selector: #selector(applyCurrentSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // apply current settings
        applyCurrentSettings()
        // check is default language is selected
        checkLanguage()
    }
    
    @objc func checkLanguage() {
        if(!settings.first_run!) {
            
            self.showMessageDialog(
                title: ProjectText.questionLang[0].localized(lang: settings.app_language!),
                message: ProjectText.questionLang[1].localized(lang: settings.app_language!),
                okEvent: { (action: UIAlertAction!) in self.presentLanguage() }
            )
        }
    }
    
    
    @objc func applyCurrentSettings() {
        // read current settings
        settings.readSettings()
        
        // set navigation bar  title
        navigationItem.title = ProjectText.app_main_name.localized(lang: settings.app_language!)
        
        // set content
        var pos = 0
        for item in self.viewControllers! {
            item.title = ProjectText.tabBar[pos].localized(lang: settings.app_language!)
            pos += 1
        }
    }
    
    @objc func presentMenu() {
        
        let menuOptions =   [
            ProjectText.navigation[0].localized(lang: settings.app_language!),
            ProjectText.navigation[1].localized(lang: settings.app_language!),
            ProjectText.navigation[2].localized(lang: settings.app_language!),
            ProjectText.navigation[3].localized(lang: settings.app_language!),
            ProjectText.navigation[4].localized(lang: settings.app_language!),
        ]
        let menuImages =    [
            "ic_action_share",
            "ic_action_rate",
            "ic_action_privacy_policy",
            "ic_action_all_apps",
            "ic_action_help",
        ]
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        
        var actions: Array<PopMenuDefaultAction> = Array<PopMenuDefaultAction>()
        
        actions.append(PopMenuDefaultAction(title: menuOptions[0], image: UIImage(named: menuImages[0])))
        actions.append(PopMenuDefaultAction(title: menuOptions[1], image: UIImage(named: menuImages[1])))
        actions.append(PopMenuDefaultAction(title: menuOptions[2], image: UIImage(named: menuImages[2])))
        actions.append(PopMenuDefaultAction(title: menuOptions[3], image: UIImage(named: menuImages[3])))
        actions.append(PopMenuDefaultAction(title: menuOptions[4], image: UIImage(named: menuImages[4])))
        actions.append(PopMenuDefaultAction(title: "ver: " + appVersion!, image: UIImage(named: "left_menu_version")))
        
        menuViewController = PopMenuViewController(actions: actions)
        menuViewController.appearance.popMenuStatusBarStyle = UIStatusBarStyle.lightContent
        menuViewController.appearance.popMenuItemSeparator = .fill()
        
        // The manual way
        menuViewController.appearance.popMenuActionCountForScrollable = 10 // default 6
        menuViewController.appearance.popMenuScrollIndicatorHidden = true // default false
        
        menuViewController.delegate = self
        
        present(menuViewController, animated: true, completion: nil)
    }
    
    @objc func presentLanguage() {
        
        let menuOptions =   [
            ProjectText.language[0].localized(lang: settings.app_language!),
            ProjectText.language[1].localized(lang: settings.app_language!),
            ProjectText.language[2].localized(lang: settings.app_language!),
        ]
        
        var actions: Array<PopMenuDefaultAction> = Array<PopMenuDefaultAction>()
        
        actions.append(PopMenuDefaultAction(title: menuOptions[0]))
        actions.append(PopMenuDefaultAction(title: menuOptions[1]))
        actions.append(PopMenuDefaultAction(title: menuOptions[2]))
        
        languageViewController = PopMenuViewController(actions: actions)
        languageViewController.appearance.popMenuStatusBarStyle = UIStatusBarStyle.lightContent
        languageViewController.appearance.popMenuItemSeparator = .fill()
        
        // The manual way
        languageViewController.appearance.popMenuActionCountForScrollable = 10 // default 6
        languageViewController.appearance.popMenuScrollIndicatorHidden = true // default false
        
        languageViewController.delegate = self
        
        languageViewController.didDismiss = { selected in
            // `selected` is a bool indicating if a selection has been made
            
            if !selected {
                // When the user tapped outside of the menu
                self.checkLanguage()
            }
        }
        
        present(languageViewController, animated: true, completion: nil)
    }
    
    @objc func shareApp() {
        openShareApp = true
        openParentalGate()
    }
    
    @objc func rateApp() {
        openRateApp = true
        openParentalGate()
    }
    
    @objc func privacyPolicyApp() {
        openPrivacyPolicy = true
        openParentalGate()
    }
    
    @objc func allApps() {
        openAppStore = true
        openParentalGate()
    }
    
    @objc func openHowTo() {
        // play help sound
        ApplicationWork.delayWithSeconds(1) {
            self.player.Play(soundName: "user_guide_action", loop: false, localized: true)
        }
        
        // open slider with index
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "SliderViewController") as? SliderViewController
        
        vc?.subcategoryID = 0
        self.navigationController?.pushViewController(vc!, animated:true)
    }
    
    @objc func logout() {
        showMessageDialog(title: "Выход", message: "Вы хотите выйти из данного аккаунта?",
                          okEvent: { (action: UIAlertAction!) in
                            
                            _ = self.navigationController?.popToRootViewController(animated: true)
                            
        },
                          cancelEvent: nil)
    }
    
    @objc func setLanguage(_ index: Int) {
        var lang: String!
        
        switch index {
        case 0:
            lang = "ru"
            break
        case 1:
            lang = "en"
            break
        case 2:
            lang = "ro"
            break
        default:
            break
        }
        
        if let language = lang {
            settings.setLanguage(lang: language)
            settings.setFirstRun(state: true)
            
            resetApp()
        }
    }
    
    
    func resetApp() {
        let appDelegate = AppDelegate()
        appDelegate.resetApp()
    }
    
    func exitApp() {
        // home button pressed programmatically - to thorw app to background
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        // terminaing app in background
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            exit(EXIT_SUCCESS)
        })
    }
    
}

extension MainTabBarController: PopMenuViewControllerDelegate {
    
    func popMenuDidSelectItem(_ popMenuViewController: PopMenuViewController, at index: Int) {
        // Do stuff here...
        popMenuViewController.dismiss(animated: true, completion: nil)
        
        if(popMenuViewController == menuViewController) {
            switch index {
            case 0:
                shareApp()
                break
            case 1:
                rateApp()
                break
            case 2:
                privacyPolicyApp()
                break
            case 3:
                allApps()
                break
            case 4:
                openHowTo()
                break
            default: break
            }
        }
        else if(popMenuViewController == languageViewController) {
            setLanguage(index)
        }
    }
    
}

extension MainTabBarController: ParentalGateControllerDelegate {
    
    func openParentalGate() {
        
        // open Parental Gate PopUp Dialog
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "ParentalGateController") as? ParentalGateController
        
        vc?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc?.delegate = self
        vc?.app_language = settings.app_language!
        self.present(vc!, animated:true, completion: nil)
    }
    
    func ParentalGateControllerResponse(success: Bool) {
        
        if success {
            if openShareApp {
                openShareApp = false
                
                ApplicationWork.delayWithSeconds(1) {
                    let message = ProjectText.share[1].localized(lang: self.settings.app_language!)
                    let image = UIImage(named: "AppIcon")
                    
                    if let link = URL(string: ProjectText.appUrl[0]) {
                        //Enter link to your app here
                        let objectsToShare = [message, image!, link] as [Any]
                        
                        let shareActivity = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        //New Excluded Activities Code
                        shareActivity.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                        shareActivity.popoverPresentationController?.sourceView = self.view
                        self.self.present(shareActivity, animated: true, completion: nil)
                        /*
                         if let shareActivity = ApplicationWork.shareApp(message: ProjectText.share[1].localized(lang: settings.app_language!)) {
                         self.present(shareActivity, animated: true, completion: nil)
                         } */
                    }
                }
            }
            else if openRateApp {
                openRateApp = false
                ApplicationWork.requestReviewManually()
            }
            else if openPrivacyPolicy {
                ApplicationWork.openUrl(url: "privacy".localized(lang: self.settings.app_language!))
                openPrivacyPolicy = false
            }
            else if openAppStore {
                openAppStore = false
                ApplicationWork.openAppStore()
            }
            
        }
        
    }
}
